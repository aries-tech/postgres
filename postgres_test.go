package store

import (
	"context"
	"database/sql"
	"fmt"
	"net"
	"net/mail"
	"testing"
	"time"

	// "git.nops.corp.pvt/raven/services/tickets/models"
	// "github.com/google/uuid"
)

const testDSN = "postgres://postgres:postgres@localhost:5432/incidents_test?sslmode=disable"

func seedProblemTable(t *testing.T, db *sql.DB) uuid.UUID {
	t.Helper()
	name := "link down"
	desc := "link down"
	var id uuid.UUID
	err := db.QueryRowContext(context.TODO(), queryInsertProblem,
		time.Now(),
		time.Now(),
		name,
		desc,
	).Scan(&id)
	if err != nil {
		t.Fatalf("insert problem failed: %v", err)
	}
	return id
}

func seedServiceTable(t *testing.T, db *sql.DB) uuid.UUID {
	t.Helper()
	name := "DATA"
	tp := "COPPER"
	speed := "7.100M/768K"
	var id uuid.UUID
	err := db.QueryRowContext(context.TODO(), queryInsertService,
		time.Now(),
		time.Now(),
		name,
		tp,
		speed,
	).Scan(&id)
	if err != nil {
		t.Fatalf("insert service failed: %v", err)
	}
	return id
}

func seedAffectedCustomerTable(t *testing.T, db *sql.DB) uuid.UUID {
	t.Helper()
	fn := "first"
	ln := "last"
	email := "first@last.com"
	clli := "1234578910"
	port := "1"
	btn := "123-456-7890"
	wtn := "123-456-7890"
	var id uuid.UUID
	err := db.QueryRowContext(context.TODO(), queryInsertAffectedCustomer,
		time.Now(),
		time.Now(),
		fn,
		ln,
		email,
		clli,
		port,
		btn,
		wtn,
	).Scan(&id)
	if err != nil {
		t.Fatalf("insert affected customer failed: %v", err)
	}
	return id
}

func seedDevicesTable(t *testing.T, db *sql.DB) uuid.UUID {
	t.Helper()
	hn := "test.host.1"
	clli := "12345678910"
	ipv4 := net.ParseIP("1.1.1.1")
	var id uuid.UUID
	err := db.QueryRowContext(context.TODO(), queryInsertDevice,
		time.Now(),
		time.Now(),
		hn,
		clli,
		ipv4.String(),
	).Scan(&id)
	if err != nil {
		t.Fatalf("insert devices failed: %v", err)
	}
	return id
}

func seedCategoriesTable(t *testing.T, db *sql.DB) uuid.UUID {
	t.Helper()
	name := "degraded"
	var id uuid.UUID
	err := db.QueryRowContext(context.TODO(), queryInsertCategory,
		time.Now(),
		time.Now(),
		name,
	).Scan(&id)
	if err != nil {
		t.Fatalf("insert category failed: %v", err)
	}
	return id
}

func seedEventsTable(t *testing.T, db *sql.DB) uuid.UUID {
	t.Helper()
	var id uuid.UUID
	err := db.QueryRowContext(context.TODO(), queryInsertEvent,
		uuid.New(),
		time.Now(),
		seedCategoriesTable(t, db),
		seedDevicesTable(t, db),
		"link down in lag",
		"xe-0/0/0 down in ae0",
	).Scan(&id)
	if err != nil {
		t.Fatalf("insert event failed: %v", err)
	}
	return id
}

func cleanupTable(t *testing.T, tbl string) {
	t.Helper()
	db, err := getDB(context.TODO(), testDSN)
	if err != nil {
		t.Fatalf("connection to db failed: %v", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()
	_, err = db.ExecContext(ctx, fmt.Sprintf("DELETE FROM %v;", tbl))
	if err != nil {
		t.Fatalf("clean up failed: %v", err)
	}
}

// TestNewPostgresRepo tests the postgres factory func to
// ensure we can connect to the DB successfully
// and, when it fails, return meaningful errors.
func TestNewPostgresRepo(t *testing.T) {
	testCases := []struct {
		desc string
		dsn  string
		err  error
	}{
		{
			desc: "dsn success",
			dsn:  testDSN,
			err:  nil,
		},
		{
			desc: "dsn wrong db",
			dsn:  "postgres://postgres:postgres@localhost:5432/blah?sslmode=disable",
			err:  ErrDBNoesNotExist,
		},
		{
			desc: "dsn wrong creds",
			dsn:  "postgres://postgres:blah@localhost:5432/incidents_test?sslmode=disable",
			err:  ErrInvalidCreds, // perhaps test for granularity; specific test for user/pass.
		},
		{
			desc: "dsn wrong no driver",
			dsn:  "postgres:postgres@localhost:5432/incidents_test",
			err:  ErrParsingDSN, // perhaps test for granularity; specific test for user/pass.
		},
		{
			desc: "dsn wrong format",
			dsn:  "://postgres:blah@localhost:5432/incidents_test",
			err:  ErrParsingDSN, // perhaps test for granularity; specific test for user/pass.
		},
		{
			desc: "dsn wrong port",
			dsn:  "://postgres:blah@localhost:53/incidents_test",
			err:  ErrParsingDSN, // perhaps test for granularity; specific test for user/pass.
		},
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			repo, err := NewPostgresRepo(tc.dsn)
			if err != tc.err {
				t.Errorf("got %v; want %v", err, tc.err)
			}
			_ = repo
		})
	}
}

func TestCreateProblem(t *testing.T) {
	testCases := []struct {
		desc    string
		problem models.Problem
		err     error
	}{
		{
			desc: "create problem",
			problem: models.Problem{
				CreatedAt:  time.Now(),
				ModifiedAt: time.Now(),
				Name:       "link down",
				Desc:       "link down",
			},
			err: nil,
		},
		{
			desc: "create problem dup",
			problem: models.Problem{
				CreatedAt:  time.Now(),
				ModifiedAt: time.Now(),
				Name:       "link down",
				Desc:       "link down",
			},
			err: ErrDuplicateProblem,
		},
	}
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "problems")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			got, err := repo.CreateProblem(context.Background(), tC.problem)
			if err != tC.err {
				t.Error(err)
			}
			if err == nil {
				if got.Desc != tC.problem.Desc {
					t.Errorf("got %v; want %v", got.Desc, tC.problem.Desc)
				}
				if got.ID.String() == "" {
					t.Error("got empty id")
				}
			}
		})
	}
}

func TestGetProblem(t *testing.T) {
	testCases := []struct {
		desc    string
		problem models.Problem
		err     error
	}{
		{
			desc: "get problem success",
			problem: models.Problem{
				CreatedAt:  time.Now(),
				ModifiedAt: time.Now(),
				Name:       "name",
				Desc:       "link down",
			},
			err: nil,
		},
		{
			desc: "get problem no uuid",
			problem: models.Problem{
				CreatedAt:  time.Now(),
				ModifiedAt: time.Now(),
				Name:       "no uuid",
				Desc:       "no uuid",
			},
			err: ErrProblemDoesNotExist,
		},
	}
	// seed the problem table
	db, err := getDB(context.Background(), testDSN)
	if err != nil {
		t.Fatalf("table seed failed: %v", err)
	}
	// Getting the ID of the seeded entry and tacking it onto the
	// first testcase.
	// TODO: this feels wrong: need a better solution.
	id := seedProblemTable(t, db)
	testCases[0].problem.ID = id
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "problems")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			got, err := repo.GetProblemByID(context.TODO(), tC.problem.ID)
			if err != tC.err {
				t.Errorf("got %v; want %v", err, tC.err)
			}
			if got.ID != tC.problem.ID { // this condition passes for the no uuid example b/c they're both nil UUID values.
				t.Errorf("got %v; want %v", got.ID, tC.problem.ID)
			}
		})
	}
}

func TestUpdateProblem(t *testing.T) {
	// seed the problem table
	db, err := getDB(context.Background(), testDSN)
	if err != nil {
		t.Fatalf("table seed failed: %v", err)
	}
	want := models.Problem{
		ID:         seedProblemTable(t, db),
		Name:       "modified problem",
		Desc:       "modified problem",
		ModifiedAt: time.Now(),
	}
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "problems")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	got, err := repo.UpdateProblem(context.TODO(), want)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	if got.Name != want.Name {
		t.Errorf("got %v; want %v", got.Name, want.Name)
	}
}

func TestDeleteProblem(t *testing.T) {
	// seed the problem table
	db, err := getDB(context.Background(), testDSN)
	if err != nil {
		t.Fatalf("table seed failed: %v", err)
	}
	id := seedProblemTable(t, db)
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "problems")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	err = repo.DeleteProblem(context.TODO(), id)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
}

func TestDestroyProblem(t *testing.T) {
	// seed the problem table
	db, err := getDB(context.Background(), testDSN)
	if err != nil {
		t.Fatalf("table seed failed: %v", err)
	}
	id := seedProblemTable(t, db)
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "problems")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	err = repo.DestroyProblem(context.TODO(), id)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
}

// SERVICE TESTS
func TestCreateService(t *testing.T) {
	testCases := []struct {
		desc string
		svc  models.Service
		err  error
	}{
		{
			desc: "create service",
			svc:  models.Service{Name: "DATA", Type: "COPPER", Speed: "7.100M/768K"},
			err:  nil,
		},
		{
			desc: "create service dup",
			svc:  models.Service{Name: "DATA", Type: "COPPER", Speed: "7.100M/768K"},
			err:  ErrDuplicateService,
		},
	}
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "services")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			got, err := repo.CreateService(context.Background(), tC.svc)
			if err != tC.err {
				t.Error(err)
			}
			if err == nil {
				if got.Name != tC.svc.Name {
					t.Errorf("got %v; want %v", got.Name, tC.svc.Name)
				}
				if got.ID.String() == "" {
					t.Error("got empty id")
				}
			}
		})
	}
}

func TestGetService(t *testing.T) {
	testCases := []struct {
		desc string
		svc  models.Service
		err  error
	}{
		{
			desc: "get service success",
			svc: models.Service{
				CreatedAt:  time.Now(),
				ModifiedAt: time.Now(),
				Name:       "DATA",
			},
			err: nil,
		},
		{
			desc: "get service no uuid",
			svc:  models.Service{},
			err:  ErrServiceDoesNotExist,
		},
	}
	// seed the affected customer table
	db, err := getDB(context.Background(), testDSN)
	if err != nil {
		t.Fatalf("table seed failed: %v", err)
	}
	// Getting the ID of the seeded entry and tacking it onto the
	// first testcase.
	// TODO: this feels wrong: need a better solution.
	id := seedServiceTable(t, db)
	testCases[0].svc.ID = id
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "services")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			got, err := repo.GetServiceByID(context.TODO(), tC.svc.ID)
			if err != tC.err {
				t.Errorf("got %v; want %v", err, tC.err)
			}
			if got.ID != tC.svc.ID { // this condition passes for the no uuid example b/c they're both nil UUID values.
				t.Errorf("got %v; want %v", got.ID, tC.svc.ID)
			}
		})
	}
}

// CUSTOMER TESTS
func TestCreateAffectedCustomer(t *testing.T) {
	testCases := []struct {
		desc             string
		affectedCustomer models.AffectedCustomer
		err              error
	}{
		{
			desc: "create affected customer",
			affectedCustomer: models.AffectedCustomer{
				FirstName: "bill",
				LastName:  "dabear",
				Email:     mail.Address{Address: "bill@dabear.com"},
				Port:      "42",
				CLLI:      "1234578910",
				IPv4:      net.ParseIP("1.1.1.1"),
				WTN:       "123-456-7890",
				BTN:       "123-456-7890",
				Services:  []models.Service{},
			},
			err: nil,
		},
		{
			desc: "create affected customer dup",
			affectedCustomer: models.AffectedCustomer{
				FirstName: "bill",
				LastName:  "dabear",
				Email:     mail.Address{Address: "bill@dabear.com"},
				Port:      "42",
				CLLI:      "1234578910",
				IPv4:      net.ParseIP("1.1.1.1"),
				WTN:       "123-456-7890",
				BTN:       "123-456-7890",
				Services:  []models.Service{},
			},
			err: ErrDuplicateAffectedCustomer,
		},
	}
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "services")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			got, err := repo.CreateAffectedCustomer(context.Background(), tc.affectedCustomer)
			if err != tc.err {
				t.Errorf("got %v; want %v", err, tc.err)
			}
			if err == nil {
				if got.FirstName != tc.affectedCustomer.FirstName {
					t.Errorf("got %v; want %v", got.FirstName, tc.affectedCustomer.FirstName)
				}
				if got.ID.String() == "" {
					t.Error("got empty id")
				}
			}
		})
	}
}

func TestGetAffectedCustomer(t *testing.T) {
	testCases := []struct {
		desc             string
		affectedCustomer models.AffectedCustomer
		err              error
	}{
		{
			desc: "get affected customer success",
			affectedCustomer: models.AffectedCustomer{
				CreatedAt:  time.Now(),
				ModifiedAt: time.Now(),
				FirstName:  "first",
				LastName:   "last",
				Email:      mail.Address{Address: "first@last.com"},
				Port:       "1",
				CLLI:       "1234578910",
				IPv4:       net.ParseIP("1.1.1.1"),
				WTN:        "123-456-7890",
				BTN:        "123-456-7890",
				Services:   []models.Service{}, // TODO: adde services!
			},
			err: nil,
		},
		{
			desc:             "get affected customer no uuid",
			affectedCustomer: models.AffectedCustomer{},
			err:              ErrAffectedCustomerDoesNotExist,
		},
	}
	// seed the affected_customer table
	db, err := getDB(context.Background(), testDSN)
	if err != nil {
		t.Fatalf("table seed failed: %v", err)
	}
	// Getting the ID of the seeded entry and tacking it onto the
	// first testcase.
	// TODO: this feels wrong: need a better solution.
	id := seedAffectedCustomerTable(t, db)
	testCases[0].affectedCustomer.ID = id
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "affected_customers")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			got, err := repo.GetAffectedCustomerByID(context.TODO(), tc.affectedCustomer.ID)
			if err != tc.err {
				t.Errorf("got %v; want %v", err, tc.err)
			}
			if got.ID != tc.affectedCustomer.ID { // this condition passes for the no uuid example b/c they're both nil UUID values.
				t.Errorf("got %v; want %v", got.ID, tc.affectedCustomer.ID)
			}
		})
	}
}

// DEVICE TESTS
func TestCreateDevice(t *testing.T) {
	testCases := []struct {
		desc   string
		device models.Device
		err    error
	}{
		{
			desc: "create device",
			device: models.Device{
				Hostname: "test.dev.host",
				CLLI:     "1234578910",
				IPv4:     net.ParseIP("1.1.1.1"),
			},
			err: nil,
		},
		{
			desc: "create device dup",
			device: models.Device{
				Hostname: "test.dev.host",
				CLLI:     "1234578910",
				IPv4:     net.ParseIP("1.1.1.1"),
			},
			err: ErrDuplicateDevice,
		},
	}
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "services")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			got, err := repo.CreateDevice(context.Background(), tC.device)
			if err != tC.err {
				t.Error(err)
			}
			if err == nil {
				if got.CLLI != tC.device.CLLI {
					t.Errorf("got %v; want %v", got.CLLI, tC.device.CLLI)
				}
				if got.ID.String() == "" {
					t.Error("got empty id")
				}
			}
		})
	}
}

func TestGetDevice(t *testing.T) {
	testCases := []struct {
		desc   string
		device models.Device
		err    error
	}{
		{
			desc:   "get device success",
			device: models.Device{},
			err:    nil,
		},
		{
			desc:   "get device no uuid",
			device: models.Device{},
			err:    ErrDeviceDoesNotExist,
		},
	}
	// seed the affected_customer table
	db, err := getDB(context.Background(), testDSN)
	if err != nil {
		t.Fatalf("table seed failed: %v", err)
	}
	// Getting the ID of the seeded entry and tacking it onto the
	// first testcase.
	// TODO: this feels wrong: need a better solution.
	id := seedDevicesTable(t, db)
	testCases[0].device.ID = id
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "devices")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			got, err := repo.GetDeviceByID(context.TODO(), tC.device.ID)
			if err != tC.err {
				t.Errorf("got %v; want %v", err, tC.err)
			}
			if got.ID != tC.device.ID { // this condition passes for the no uuid example b/c they're both nil UUID values.
				t.Errorf("got %v; want %v", got.ID, tC.device.ID)
			}
		})
	}
}

// EVENTS
func TestCreateEvent(t *testing.T) {
	// seed the affected_customer table
	db, err := getDB(context.Background(), testDSN)
	if err != nil {
		t.Fatalf("table seed failed: %v", err)
	}
	testCases := []struct {
		desc  string
		event models.Event
		err   error
	}{
		{
			desc: "create event",
			event: models.Event{
				Category: models.Category{ID: seedCategoriesTable(t, db)},
				Device:   models.Device{ID: seedDevicesTable(t, db)},
			},
			err: nil,
		},
	}
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "devices")
	defer cleanupTable(t, "categories")
	defer cleanupTable(t, "events")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			got, err := repo.CreateEvent(context.Background(), tc.event)
			if err != tc.err {
				t.Error(err)
			}
			if err == nil {
				if got.Category.ID != tc.event.Category.ID {
					t.Errorf("got %v; want %v", got.Category.ID, tc.event.Category.ID)
				}
				if got.ID.String() == "" {
					t.Error("got empty id")
				}
			}
		})
	}
	// time.Sleep(30 * time.Second)
}

func TestGetEvent(t *testing.T) {
	// seed the affected_customer table
	db, err := getDB(context.Background(), testDSN)
	if err != nil {
		t.Fatalf("table seed failed: %v", err)
	}
	testCases := []struct {
		desc  string
		event models.Event
		err   error
	}{
		{
			desc: "get event success",
			event: models.Event{
				ID: seedEventsTable(t, db),
			},
			err: nil,
		},
		{
			desc:  "get event no uuid",
			event: models.Event{},
			err:   ErrEventDoesNotExist,
		},
	}
	// ensure we delete all test rows from the table
	// after each test run.
	defer cleanupTable(t, "devices")
	defer cleanupTable(t, "categories")
	defer cleanupTable(t, "events")
	repo, err := NewPostgresRepo(testDSN)
	if err != nil {
		t.Errorf("got %v; want nil", err)
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			// time.Sleep(30 * time.Second)
			got, err := repo.GetEventByID(context.TODO(), tC.event.ID)
			if err != tC.err {
				t.Errorf("got %v; want %v", err, tC.err)
			}
			if err == nil {
				if got.ID != tC.event.ID { // this condition passes for the no uuid example b/c they're both nil UUID values.
					t.Errorf("got %v; want %v", got.ID, tC.event.ID)
				}
			}
		})
	}
}
